# kaniko

```yaml
stages:
  - 🐳build

include:
  - project: 'simplify-devops/kaniko'
    file: 'kaniko.gitlab-ci.yml'

#-----------------------------------------------------------------------------------------
# Kaniko build
#-----------------------------------------------------------------------------------------

📦:kaniko:build:
  stage: 🐳build
  extends: .kaniko:builder
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID
```